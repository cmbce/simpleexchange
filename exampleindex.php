<?php 
// determine sessionid and if none defined, recall with a new session id
$sessionid = isset($_GET['sessionid']) ? $_GET['sessionid'] : '';
if($sessionid == ""){
  $sessionid = uniqid();
  header("Location: ".$_SERVER['PHP_SELF']."?sessionid=".$sessionid);
}
// echo 'sessionid:'.$sessionid;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Comet demo</title>
    <script type="text/javascript" src="simpleexchange/jquery-1.12.0.min.js"></script>

    <script type="text/javascript">
var sessionid = "<?php echo $sessionid; ?>";
var serverAvailable = true;
    </script>
    <script type="text/javascript" src="simpleexchange/simpleExchangeAPI.js"></script>
    </head>
  <body>

<div id="content">
</div>
<div class="shareable" id="word"> Click this div to edit it </div>
<div class="shareable" id="message"> Click this div to edit it </div>
<div class="shareable" id="forTheWorld"> Click this div to edit it </div>
</body>
</html>
