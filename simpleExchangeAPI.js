// the comet/prototype script, using jquery
// the auto-update script, updates a local allData and the server
// note: this works only for DOM elements defined at the beginning
// if new "changeable" elements are added, need to manually:
// - initialize allData[[this.id]]
//        initAllData(this);
// - set the listener 
//        $('#"+this.id).blur(function() {editableContentOut(this.id);return false;});
// Note: forms should not be handled that way, they mess it up quite a bit

// *** parameters
var serverPage = './simpleexchange/backend.php';
var editedColor = "lightblue";
var backFromServerColor = "white";
var shareableClass = "shareable";
var initEditable = true; 
var colorEditable = true;

// *** treatment functions for value/key system should be customized to handle whatever you through at it
// the function launched when receiving key/value from server
// namespace for simpleExchange: se
(function( se, $, undefined ) {
    // public function
    se.randomString = function (length){
        var N = length;
        var out = Array(N+1).join((Math.random().toString(36)+'00000000000000000').slice(2, 18)).slice(0, N);
        return out;
    }

    //Private Method
    function addItem( item ) {
        if ( item !== undefined ) {
            console.log( "Adding " + $.trim(item) );
        }
    }

    //Private Property
    var isHot = true;

    //Public Property
    se.userId = se.randomString(10);

}( window.se = window.se || {}, jQuery ));

function changeIfColorSet(itemId,color){
    if(color != ''){
        $("#"+itemId).css('color', color);
    }
}

function msgTreatment(jsonIn){
    var itemId = jsonIn['itemid'];
    var itemValue = jsonIn['itemvalue'];
    var itemOp = jsonIn['itemop'];
    if(itemValue != null){
        if(itemOp == "html"){
            allData[itemId] = itemValue;
            $("#"+itemId).html(itemValue);
        }else{
            // call hook in itemop
            eval(itemOp)(itemId,itemValue);
        }
    }
}
// the function launched when getting out of a shareable content
function OutOfEditableContent(itemId){
    var currentEditable = $("#"+itemId).attr("contentEditable");
    if(currentEditable == "true"){
        var currentContent = $("#"+itemId).html();
        allData[[itemId]] = currentContent;
        sendToServer(itemId,currentContent);
        if(colorEditable){
            sendToServer(itemId+'-Editable',initEditable,"toggleEditable");
            changeIfColorSet(itemId,"black");
        }
    }
    // if not editable do nothing: an other client is doing his things
}
// launched when getting in editable content
function InEditableContent(itemId){
    if(colorEditable){
        var currentEditable = $("#"+itemId).attr("contentEditable");
        if(currentEditable == "true"){
            // disable remote
            sendToServer(itemId+'-Editable',false,"toggleEditable");
            changeIfColorSet(itemId,"grey");
            // in not editable do nothing: an other client is doing his things
        }
    }
}
function toggleEditable(itemIdEdit,value,origin){
    var itemId = itemIdEdit.split("-")[0];
    if(value=="true"){
        col = "black";
        $("#"+itemId).attr("contentEditable",true);
        $("#"+itemId).attr("readonly",false)
    }else{
        col = "blue";
        $("#"+itemId).attr("contentEditable",false);
        $("#"+itemId).attr("readonly",true)
    }
    changeIfColorSet(itemId,col);
}

var allData = {};

// receiving part
function waitForMsg(){
    if(serverAvailable){
        var request = {};
        request.sessionid = sessionid;
        request.operation = "listen"; // additional flag for the backend
        request.cookieorig = se.userId;
        $.ajax({
            type: "POST",
            url: serverPage,
            data:request,
            async: true,
            cache: false,
            timeout:10000, // milliseconds
            success: function(data){
                var json = eval('('+data+')');
                if(json['itemid'] != ""){
                    msgTreatment(json);
                    // alert(json['msg']);
                }
                // waitForMsg();
                setTimeout('waitForMsg()',10);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                if(textStatus==="timeout") {
                    // safely ignore it
                }else if(errorThrown != ""){
                    alert("error: " + textStatus + " (" + errorThrown + ")");
                }
                setTimeout('waitForMsg()',1000);
            }
        });
    }
}
// sending part 
function sendToServer(itemId,itemValue,itemop,operation,async){
    if(serverAvailable){
        var data = {};
        data.sessionid = sessionid;
        data.itemid = itemId;
        data.itemvalue = itemValue;
        if(operation === undefined){
            data.operation = "update";
        }else{
            data.operation = operation;
        }
        if(async === undefined){
            async = true;
        }
        if(itemop === undefined){
            data.itemop = "html";
        }else{
            data.itemop = itemop;
        }
        data.cookieorig = se.userId;

        $.ajax({
            type: "POST",
            url: serverPage,
            async: async,
            data: data,
            cache: false,
            timeout:4000, // milliseconds
            success: function(dataBack){
                // alert(dataBack);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                alert("error: " + textStatus + " (" + errorThrown + ")");
            }
        });
    }
    return false;
}

function initAllData(element){
    if(element.id == ""){
        alert("No id for content: "+element.textContent+". Will not handle editing.");
    }else if(typeof allData[[element.id]] !== "undefined"){
        alert("Duplicated id: "+element.id+". Will not handle editing.");
    }else{
        if(element.contentEditable == "inherit"){
            element.contentEditable = initEditable;
        }
        element.tabIndex = 0; // needed for focus to be triggered, allows tab navigation
        // allData[[element.id]] = element.textContent;
        GetFromServer(element.id);
    }
}

function GetFromServer(itemId){
    if(serverAvailable){
        var data = {};
        data.sessionid = sessionid;
        data.itemid = itemId;
        data.operation = "get";

        var prom = $.ajax({
            type: "POST",
            url: serverPage,
            async: true,
            data: data,
            cache: false,
            success: function(data){
                var json = eval('('+data+')');
                if(json['itemid'] != ""){
                    msgTreatment(json);
                    // alert(json['msg']);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                alert("error: " + textStatus + " (" + errorThrown + ")");
            }
        });
    }
    return prom;
}


// initialize
$(document).ready(function(){
    // purge sync queue
    sendToServer("","","pit");

    // init values from server
    $('.'+shareableClass).each(function() {initAllData(this)});

    // set listeners on focus
    $('.'+shareableClass).focus(function() {InEditableContent(this.id);return false;});
    
    // set listeners on change
    $('.'+shareableClass).blur(function() {OutOfEditableContent(this.id);return false;});
    
    // tell everybody when leaving
    $(window).unload(function(){
        sendToServer("","","","leaving",async=false);// asynx = false Necessary
    });
    // set listeners from out
    waitForMsg();
});
