<?php
// for debugging
ini_set('display_errors', 'On');
error_reporting(E_ALL|E_STRICT);
$usleepTime =   100000; // in microseconds
$giveUpTime =  3000000; // in microseconds MUST BE <<< than client timeout, or 
                        // a zombie process can empty the queue
// note to debug, usefull commands on redis-cli
// keys 56d6cc40761bf_*
// smembers 56d6cc40761bf_SEclientSet
// lrange 56d6cc40761bf:rq3679f3iv 0 100

// redis loading
// - store everything that comes, using key: sessionid_itemid
// - retrieving is based on the asker to know all items in sessionid
// - will need a way to POST everything at once
require "predis/autoload.php";
Predis\Autoloader::register();
try {
        $redis = new Predis\Client();
}
catch (Exception $e) {
    die($e->getMessage());
}

// get the POST values
$sessionId = isset($_POST['sessionid']) ? $_POST['sessionid'] : '';
$itemId = isset($_POST['itemid']) ? $_POST['itemid'] : '';
$itemValue = isset($_POST['itemvalue']) ? $_POST['itemvalue'] : '';
$itemOp = isset($_POST['itemop']) ? $_POST['itemop'] : FALSE;
$operation = isset($_POST['operation']) ? $_POST['operation'] : FALSE;
$cookieOrig = isset($_POST['cookieorig']) ? $_POST['cookieorig'] : FALSE;
$clientSetName = $sessionId."_SEclientSet";

// // log
// file_put_contents('backend.log',$sessionId.":",FILE_APPEND);
// file_put_contents('backend.log',$operation.":",FILE_APPEND);
// file_put_contents('backend.log',$itemId.":",FILE_APPEND);
// file_put_contents('backend.log',$itemValue.":",FILE_APPEND);
// file_put_contents('backend.log',$cookieOrig.PHP_EOL,FILE_APPEND);

$filename  = '/tmp/data'.$sessionId.'.txt';
$response = array();
if ($operation == "update" || $operation == "save"){
    // store value in redis
    $sessionItemId = $sessionId.':'.$itemId;
    $redis->hset($sessionItemId,'itemValue',$itemValue);
    $redis->hset($sessionItemId,'itemOp',$itemOp);
    // $redis->expireat($sessionItemId, strtotime("+1 week"));
    if($operation == "update"){
        // add info to the current queues
        $clientSet = $redis->smembers($clientSetName); // get the set of clients
        // file_put_contents('backend.log','session:'.$sessionId.";itemId:".$itemId.";itemValue:".$itemValue.";itemOp:".$itemOp.";orig:".$cookieOrig."\n",FILE_APPEND);
        foreach ($clientSet as $client) {
            if($client != $cookieOrig){
                $sessionIdClient = $sessionId.':'.$client;
                $redis->rpush($sessionIdClient,$sessionItemId);
                // $redis->expireat($sessionIdClient, strtotime("+1 week"));
                // file_put_contents('backend.log',"queue:".$sessionIdClient.";",FILE_APPEND);
            }
        }
        // file_put_contents('backend.log',"\n",FILE_APPEND);
    }
    die();
}else if($operation == "listen"){
    // add the client to the list of clients
    $redis->sadd($clientSetName,$cookieOrig);

    // init loop
    $sessionIdClient = $sessionId.':'.$cookieOrig;
    $sessionItemId = "";

    // infinite loop until the queue is modified
    $waitTime = 0;
    while ($sessionItemId == ""){ 
        // check redis queue
        $sessionItemId = $redis->lpop($sessionIdClient);
        usleep($usleepTime); // sleep 10ms to unload the CPU
        $waitTime = $waitTime+$usleepTime;
        if($waitTime > $giveUpTime){
            $response['itemid'] = "";
            $response['error'] = "Server Time Out";
            die(json_encode($response));
        }
    }
    // return a json array
    list($sessionId,$response['itemid']) = explode(':',$sessionItemId);
    // file_put_contents('backend.log',$sessionItemId.":",FILE_APPEND);
    // $response['itemid'] = $sessionItemId;
    $response['itemvalue'] = $redis->hget($sessionItemId,'itemValue');
    $response['itemop'] = $redis->hget($sessionItemId,'itemOp');

}else if($operation == "get"){
    $response['itemid'] = $itemId;
    $sessionItemId = $sessionId.':'.$itemId;
    $response['itemvalue'] = $redis->hget($sessionItemId,'itemValue');
    $response['itemop'] = $redis->hget($sessionItemId,'itemOp');
}else if($operation == "leaving"){
    $redis->srem($clientSetName,$cookieOrig); // remove register of queue
    $sessionIdClient = $sessionId.':'.$cookieOrig;
    $redis->del($sessionIdClient); // remove queue
    $response = "Good bye!";
}else{
    $response['itemvalue'] = "unknown itemop";
}

echo json_encode($response);
flush();
?>
