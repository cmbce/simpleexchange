# README #

### What is this for ###
Simply share modifications on items of a webpage with multiple people simply by sharing a page link. Every time you edit a contenteditable div, everybody with the link sees the changes. In addition, everything is backed-up on the server. If you open the same page again later it will open with the same changes.  

### How do I use it on my webpages? ###
Once set up, to get a new field backed up and shareable just add class "shareable" to the div. That's all.

You can additionally use this system to save on the server any key/value pair you want. Just call:
sendUpdate(key,value,"some key for the type of operation to perform");

And add in msgTreatment a handler for this "itemOp" like for "html", the default handler.

Or retrieve it with:
GetFromServer(key);

An example of use of this package is the [CompEl project](https://bitbucket.org/correlmetriquepaysageetbioag/compel/overview).

### How do I get set up? ###
## Dependencies setting ##
1. Be sure to have a php server installed, on ubuntu linux, the following should do it: 

```
#!bash
sudo apt-get install lamp-server^
sudo apt-get install libapache2-mod-php5
sudo service apache2 restart
```

2. Be sure to have a [redis](http://redis.io/) server setup. On ubuntu linux it should be as simple as 

```
#!bash

sudo apt-get install redis-*
```

3. Clone this repository
4. Enable logging. The PHP server part of simpleexchange writes its logs in simpleexchange/backend.log, make this file and make it writeable for any user. On linux: 
```
#!bash
touch simpleexchange/backend.log 
chmod go+w simpleexchange/backend.log
```
5. Install the predis submodules, for linux, in simpleexchange:
```
#!bash

git submodule init
git submodule update
```

## Testing ##
You can use the [CompEl project](https://bitbucket.org/correlmetriquepaysageetbioag/compel/overview) project or more simply: 
1. Copy/link exampleindex.php from simpleExchange to its root (it will *not* work where it is)
2. Access exampleindex.php on your server. You can edit and any change will be shared with anybody with this page open (just share the link to the page, as it appears in your navigation bar). To test it, just open the same page several times in your browser. 
3. Of course make sure your website is documented in your http server...

Then you are set up, reuse the code in exampleindex.php into your webpages to add this fonctionnality. 

### How does it work?
When the DOM is ready, all div of class "changeable" are saved on server (using redis) which redistribute it to all clients connected with this session id, through long wait ajax calls (comet). 

### Contribution guidelines ###
Contributions via pull request and feedbacks using the issue tracker are more than welcome. Just try to keep it lightweight.